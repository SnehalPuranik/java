package Day2;

public class Program5 {
    public static void main(String[] args) {
        int line=4;
        int star=5;
        int ch=1;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
            {
                System.out.print(ch++);
                if(ch>7)
                    ch=1;
            }
            System.out.println();
        }
    }
}
/*
12345
67123
45671
23456
 */
