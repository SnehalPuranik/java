package Day2;

public class Program6 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        char ch='A';
        for(int i=0;i<line;i++){
            for(int j=0;j<star;j++){
                System.out.print(ch++);
                if(ch>'H')
                    ch='A';

            }
            System.out.println();
        }
    }
}
/*
ABCDE
FGHAB
CDEFG
HABCD
EFGHA

 */