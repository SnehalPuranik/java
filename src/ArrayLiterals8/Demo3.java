package ArrayLiterals8;

public class Demo3 {
    public static void main(String[] args) {
        double basicsalary=12000;
        double hra=6000;
        double pf=800;
        double incentives=3000;
        double professiontask=200;
        double netsalary=basicsalary+hra+incentives-pf-professiontask;
        double grosssalary=basicsalary+hra+incentives;
        System.out.println("A: "+netsalary);
        System.out.println("B: "+grosssalary);
    }
}
