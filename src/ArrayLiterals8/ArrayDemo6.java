package ArrayLiterals8;

public class ArrayDemo6 {
    public static void main(String[] args) {
        int qty=20;
        double price=500;
        double total=qty*price;
        //applying 10% discount
        double discountedamt=total*0.1;
        //display final amount
        double finalamt=total-discountedamt;
        //print both values
        System.out.println("TOTAL: "+total);
        System.out.println("FINAL: "+finalamt);
    }
}
