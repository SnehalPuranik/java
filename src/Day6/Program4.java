package Day6;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class Program4 {
    public static void main(String[] args) {
        int line = 7;
        int star = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++)
                if (j == star - 1 || j == star-2)
                    System.out.print("*");
                else
                    System.out.print(" ");
            System.out.println();
            if (i <= 2) {
                star +=1;
            } else {
                star -= 1;
            }

        }

    }

}
/*

 *
 **
  **
   **
  **
 **
 *

 */
