package Day13;

public class BuzzNumber {
    public static void main(String[] args) {
        for (int i = 0; i <= 10; i++) {
            int a = i;
            int b = a;
            if (a % 10 == 7 || a % 7 == 0) {
                System.out.println(b + " Is a Buzz Number");
            }
            else {
                System.out.println(b + " Is not a Buzz Number");
            }
        }
    }
}
