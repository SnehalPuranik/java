package Day13;

public class FindZeros {
    public static void main(String[] args) {
        long a=10001014;
        int count=0;
        while(a!=0){
            long r=a%10;
            if(r==0){
                count++;
            }
            a=a/10;
        }
        System.out.println(count);
    }

}
