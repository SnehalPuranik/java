package ArrayPattern;

public class ArrayThirdLowest {
    public static void main(String[] args) {
        int[] arr={1,5,7,9,10,4,8,10};
        int min1=arr[0];
        int min2=arr[1];
        int min3=arr[2];
        for(int i:arr){
            if(i<min1){
                min2=min1;
                min1=i;
            }
            else if(i<min2&&i!=min1){
                min3=min2;
                min2=i;
            }
            else if(i<min3&&i!=min2){
                min3=i;
            }
        }
        System.out.println("The min no is:"+min1);
        System.out.println("The Second min no is:"+min2);
        System.out.println("The Third min no is:"+min3);
    }
}
