package ArrayPattern;

public class BinaryConversion {
    public static void main(String[] args) {
        int a=15;
        int[] arr=new int[5];
        int i=0;
        while(a!=0){
            arr[i]=a%2;
            i++;
            a/=2;
        }
        for(int j=i;j>=0;j--){
            System.out.print(arr[j]);
        }
    }
}
