package ArrayPattern;

public class ArraySecondMax {
    public static void main(String[] args) {
        int[]arr={1,5,7,9,11,4,8,10};
        int max1=arr[0];
        int max2=arr[1];
        for(int i:arr){
            if(i>max1){
                max2=max1;
                max1=i;
            }
            else if(i>max2&&i!=max1){
                max2=i;
            }
        }
        System.out.println("The max no is:"+max1);
        System.out.println("The Second max no is:"+max2);
    }
}
