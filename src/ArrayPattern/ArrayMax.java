package ArrayPattern;

public class ArrayMax {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,5};
        int max=arr[0];
        for(int a=0;a<=arr.length;a++){
            if(a>max){
                max=a;
            }
        }
        System.out.println("The max No is: "+max);
    }
}
