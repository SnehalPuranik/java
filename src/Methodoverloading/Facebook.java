package Methodoverloading;

public class Facebook {
    String email="abc@gmail.com";
    int contact=1234567;
    String pass="ABC";
    void login(String emailId,String password){
        if(email.equalsIgnoreCase(email)){
            System.out.println("EmailId is "+email);
            System.out.println("Password is "+pass);
        }
        else{
            System.out.println("Invalid Details");
        }
    }
    void login(int contactNo,String password){
        if (contact==contactNo) {
            System.out.println("Contact Number is "+contact);
            System.out.println("Password is "+pass);
        }
        else {
            System.out.println("Invalid details");
        }
    }
}
