package Methodoverloading;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Student s1=new Student();
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Search Criteria");
        System.out.println("1.Search by Name");
        System.out.println("2.Search by Contact no");
        int choice=sc.nextInt();

        if(choice==1)
        {
            System.out.println("Enter Name");
            String name=sc.next();
            s1.search(name);
            String name1=sc.next();
            s1.search(name1);
        }
        else if(choice==2)
        {
            System.out.println("Enter Contact");
            int contact=sc.nextInt();
            s1.search(contact);
            int contact1=sc.nextInt();
            s1.search(contact1);
        }
        else {
            System.out.println("Invalid choice");
        }
    }
}