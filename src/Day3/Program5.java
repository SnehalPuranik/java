package Day3;

public class Program5 {
    public static void main(String[] args) {
        int star = 1;
        int line = 9;
        int space = 4;

        for (int i = 0; i < line; i++) {

            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            char ch ='A';
            for (int j = 0; j < star; j++) {
                System.out.print(ch+++" ");
            }
            System.out.println();
            if(i<=3)
            {
                space--;
                star++;
            }
            else {
                space++;
                star--;
            }
        }
    }
}
/*
    A
   A B
  A B C
 A B C D
A B C D E
 A B C D
  A B C
   A B
    A

 */