package Day3;

public class Program3 {
    public static void main(String[] args) {
        int star = 1;
        int line = 5;
        int space = line - 1;
        int ch = 1;
        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }

            for (int j = 0; j < star; j++) {
                if(ch>4){
                    ch=1;
                }
                System.out.print(ch++);
            }
            System.out.println();
            space--;
            star++;
        }
    }
}
/*
    1
   23
  412
 3412
34123
 */
