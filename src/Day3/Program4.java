package Day3;

public class Program4 {
    public static void main(String[] args) {
        int star = 1;
        int line = 5;
        int space = line - 1;
        int ch=5;

        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            int ch1 = ch;
            for (int j = 0; j < star; j++) {
                System.out.print(ch1++);
            }
            ch--;
            System.out.println();
            space--;
            star++;
        }
    }
}
/*
    5
   45
  345
 2345
12345
 */
