package Day3;

public class Program2 {
    public static void main(String[] args) {
        int star = 1;
        int line = 5;
        int space = line - 1;

        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            int ch = 1;
            for (int j = 0; j < star; j++) {
                System.out.print(ch++);
            }
            System.out.println();
            space--;
            star++;
        }
    }
}
/*
    1
   12
  123
 1234
12345
 */