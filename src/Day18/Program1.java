package Day18;

import java.util.Arrays;

public class Program1 {
    public static void main(String[] args) {
        int[]arr={5,3,6,2,1};
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr.length-1;j++) //we take -1 bcoz array index bound of exception
            {
                if(arr[j]>arr[j+1]){
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }

        }
        for(int a:arr)
            System.out.print(a+" ");


    }
}
